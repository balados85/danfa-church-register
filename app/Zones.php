<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zones extends Model
{
    use Notifiable;

    protected $fillable = ['name_of_leader', 'name_of_zone', 'leader_id']; 

    public function members(){

		 return $this->hasMany(Members::class);
	}

    
}
