<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donations extends Model
{
    use Notifiable;

    protected $fillable = ['amount_donated', 'date_of_donation', 'purpose_of_donation', 'recipient_id', '_is_member']; 
    
    public function donated_by(){

		 return $this->belongsTo(Members::class);
	}
}
