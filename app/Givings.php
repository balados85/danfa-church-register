<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Givings extends Model
{
    use Notifiable;

    protected $fillable = ['total_giving', 'date_recorded', 'witnessed_by']; 

    public function recorded(){

		 return $this->belongsTo(Members::class);
	}

	protected static function boot()
{
    parent::boot();

    static::creating(function ($query) {
        $query->date_recorded = Carbon::now();
    });
}
}
