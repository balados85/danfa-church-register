<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class AttendanceRegResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function getMembersinRegister($id){
        return DB::table("attendances")->where('register_id', $id)->get();
    }
    public function toArray($request)
    {
        return[
            "id" => $this->id,
            "total_attendance" => $this->total_attendance,
            "register_date" => $this->register_date,
            "week_of_year" => $this->week_of_year,
            "day_of_week" => $this->day_of_week,
            "closed" => $this->closed,
            "total_male" => $this->total_male,
            "total_female" => $this->total_female,
            "total_children" => $this->total_children,
            "total_visitors" => $this->total_visitors,
        ];
    }

    /*public function toArray($request)
    {
        return[
            "id" => $this->id,
            "total_attendance" => $this->total_attendance,
            "register_date" => $this->register_date,
            "week_of_year" => $this->week_of_year,
            "day_of_week" => $this->day_of_week,
            "closed" => $this->closed,
            "total_male" => $this->total_male,
            "total_female" => $this->total_female,
            "total_children" => $this->total_children,
            "total_visitors" => $this->total_visitors,
            "members" => $this->getMembersinRegister($this->id),
        ];
    }*/
}
