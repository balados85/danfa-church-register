<?php

namespace App\Http\Controllers;

use App\Benevolences;
use Illuminate\Http\Request;

class BenevolencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Benevolences  $benevolences
     * @return \Illuminate\Http\Response
     */
    public function show(Benevolences $benevolences)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benevolences  $benevolences
     * @return \Illuminate\Http\Response
     */
    public function edit(Benevolences $benevolences)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Benevolences  $benevolences
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benevolences $benevolences)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benevolences  $benevolences
     * @return \Illuminate\Http\Response
     */
    public function destroy(Benevolences $benevolences)
    {
        //
    }
}
