<?php

namespace App\Http\Controllers;

use App\Members;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;
use Crypt;

class MembersController extends Controller
{
    
    /**
     * Where to redirect users after adding members.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::MEMBERS;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Members::paginate(15);
         return view('/members.members')->with('members', $members);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/members/member_add');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $data)
    {
        var_dump($data);
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'othernames' => ['string', 'max:255'],
            'contactnumber' => ['string', 'max:10', 'unique:members'],
            'gender' => ['required'],
            'email' => ['string', 'email', 'max:255', 'unique:members'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'maritalstatus' => ['required'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {
         $data = Members::create([
            "first_name" => $data['firstname'],
            "last_name" => $data['lastname'],
            "other_names" => $data['othernames'],
            "gender" => $data['gender'],
            "date_of_birth" => $data['dateofbirth'],
            "baptism_date" => $data['dateofbaptism'],
            "email_addresses" => $data['email'],
            "marital_status" => $data['maritalstatus'],
            "profession" => $data['profession'],
            "home_town" => $data['hometown'],
            "place_of_birth" => $data['placeofbirth'],
            "address" => $data['houselocation'],
            "place_of_work" => $data['placeofwork'],
            "contact_number" =>$data['contactnumber'],
        ]);

        return view('/members.profile')->with('member',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function show(Members $members)
    {
        //
    }

    public function member($id){
        $member = Members::findOrFail($id);

        return redirect('/profile', $member);
    }

    public function profile($id){
        $id = Crypt::decrypt($id);
        $member = Members::findOrFail($id);
       // return view('/members.member', $member);
        return view('/members.profile')->with('member', $member);
    }

    public function activate($id, $status){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $data)
    {
        $id = $data['id'];
        $member = Members::findOrFail($id);
        $member->first_name = $data['firstname'];
        $member->last_name   =  $data['lastname'];
        $member->other_names =     $data['othernames'];
        $member->gender = $data['gender'];
        $member->date_of_birth =  $data['dateofbirth'];
        $member->baptism_date = $data['dateofbaptism'];
        $member->email_address =  $data['email'];
        $member->marital_status = $data['maritalstatus'];
        $member->profession =  $data['profession'];
        $member->home_town =  $data['hometown'];
        $member->place_of_birth =  $data['placeofbirth'];
        $member->address = $data['houselocation'];
        $member->place_of_work =  $data['placeofwork'];
        $member->contact_number = $data['contactnumber'];
        $member->status = $data['activestatus'];

        $member = $member->update();

        $members = Members::paginate(15);
         return redirect('/members')->with('members', $members);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Members $members)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function destroy(Members $members)
    {
        //
    }
}
