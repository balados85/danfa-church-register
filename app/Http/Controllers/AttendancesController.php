<?php

namespace App\Http\Controllers;

use App\Attendances;
use App\Members;
use App\AttendanceRegister;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AttendancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function show(Attendances $attendances)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $attendances)
    {
        $date = Carbon::now();

        $member_id = $attendances['member_id'];
        $register_id = $attendances['register_id'];
        $query = ['member_id' => $attendances['member_id'], 'register_id' => $attendances['register_id']];
        $attendance = Attendances::firstWhere($query);
        $attendance->time_in_church = $date->hour.':'.$date->minute;
        $attendance->late = ($attendances['late'] == null) ?  0 : 1;
        $attendance->present = ($attendances['present']  == null) ?  0 : 1;
        $attendance->update();

        $register = AttendanceRegister::findOrFail($register_id);
        $member = Members::findOrFail($member_id);

        $register->total_attendance = $register->total_attendance + 1;
        if($member->gender == 'FEMALE')
            $register->total_female = $register->total_female + 1;

        if($member->gender == 'MALE')
            $register->total_male = $register->total_female + 1;

        if($attendance->late)
            $register->total_late_attendance = $register->total_late_attendance + 1;

        if(!$attendance->late)
            $register->total_on_time_atendance = $register->total_on_time_atendance + 1;

        $register->update();

        return view('registers.attendance')->with('registers', $register);
    }

     public static function registered($member_id, $register_id){
        $bool = false;
        //$member_id = $attendances['member_id'];
        //$register_id = $attendances['register_id'];
        $query = ['member_id' => $member_id, 'register_id' => $register_id];
        $attendance = Attendances::firstWhere($query);

        if($attendance->present)
            $bool = true;

        return $bool;
    }


    public static function late($member_id, $register_id){
        $bool = false;
        //$member_id = $attendances['member_id'];
        //$register_id = $attendances['register_id'];
        $query = ['member_id' => $member_id, 'register_id' => $register_id];
        $attendance = Attendances::firstWhere($query);

        if($attendance->late)
            $bool = true;

        return $bool;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendances $attendances)
    {
        //
    }

 public function addvisitor($id){
        return view('visitors.visitor_add')->with('id', $id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendances $attendances)
    {
        //
    }
}
