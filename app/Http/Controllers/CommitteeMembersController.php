<?php

namespace App\Http\Controllers;

use App\CommitteeMembers;
use Illuminate\Http\Request;

class CommitteeMembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CommitteeMembers  $committeeMembers
     * @return \Illuminate\Http\Response
     */
    public function show(CommitteeMembers $committeeMembers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CommitteeMembers  $committeeMembers
     * @return \Illuminate\Http\Response
     */
    public function edit(CommitteeMembers $committeeMembers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CommitteeMembers  $committeeMembers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommitteeMembers $committeeMembers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CommitteeMembers  $committeeMembers
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommitteeMembers $committeeMembers)
    {
        //
    }
}
