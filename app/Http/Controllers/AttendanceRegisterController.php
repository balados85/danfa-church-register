<?php

namespace App\Http\Controllers;

use App\AttendanceRegister;
use App\Attendances;
use App\Members;
use App\Visitors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\AttentionRegisterCollection;
use App\Http\Resources\AttendanceRegResource;

class AttendanceRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendances = AttendanceRegister::paginate(10);
        return view('registers.register')->with('registers', $attendances);
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('/registers.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $register = AttendanceRegister::create([
            "total_attendance" => 0,
        ]);

        $members = Members::all();
        foreach ($members as $member) {
            Attendances::create([
                "member_id" => $member->id,
                "register_id" => $register->id,
                "late" => FALSE,
                "present" => FALSE,
                "time_in_church" => "",
            ]);
        }

       

        /*$data = [
            'members' => $members,
            'register_id' => $register->id
        ];*/

        return redirect('/register/'.$register->id)->with('members', $members);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttendanceRegister  $attendanceRegister
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $register = AttendanceRegister::findOrFail($id);
        //$members = DB::table("attendances")->where('register_id', $id)->get();

        //var_dump($register);

        return view('registers.attendance')->with('registers', $register);
    }

    public function details(Request $request){

        $register = AttendanceRegister::findOrFail($request['close']);

        $register->closed = TRUE;

        $register->update();

        $visitors = Visitors::where('register_id', $register->id)->get();

        $pass_on = [
            'registers' => $register,
            'visitors' => $visitors
        ];
      
        return view('registers.attendance_report')->with('pass_on', $pass_on);
    }

    public function close_details($id){

        $register = AttendanceRegister::findOrFail($id);

        $visitors = Visitors::where('register_id', $id)->get();

         $pass_on = [
            'registers' => $register,
            'visitors' => $visitors
        ];
      
        return view('registers.attendance_report')->with('pass_on', $pass_on);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttendanceRegister  $attendanceRegister
     * @return \Illuminate\Http\Response
     */
    public function edit(AttendanceRegister $attendanceRegister)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttendanceRegister  $attendanceRegister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttendanceRegister $attendanceRegister)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttendanceRegister  $attendanceRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttendanceRegister $attendanceRegister)
    {
        $register = AttendanceRegister::findOrFail($id);
        $register->delete();
        return view('/registers.register')->with('registers',$register);
    }
}
