<?php

namespace App\Http\Controllers;

use App\Visitors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AttendanceRegister;

class VisitorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $visitors = Visitors::paginate(15);
         return view('/visitors.visitors')->with('visitors', $visitors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = Visitors::create([
            "last_name" => $request['lastname'],
            "other_names" => $request['othernames'],
            "marital_status" => $request['maritalstatus'],
            "address" => $request['address'],
            "contact_number" =>$request['contactnumber'],
            "invited_by" =>$request['invitedby'],

        ]);

         $register = AttendanceRegister::findOrFail($request['register_id']);
         $register->total_visitors = $register->total_visitors + 1;
         $register->update();

        return redirect('/register/'.$request['register_id']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitors  $visitors
     * @return \Illuminate\Http\Response
     */
    public function show(Visitors $visitors)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitors  $visitors
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitors $visitors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitors  $visitors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitors $visitors)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitors  $visitors
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitors $visitors)
    {
        //
    }
}
