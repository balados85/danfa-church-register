<?php

namespace App\Http\Controllers;

use App\Committees;
use Illuminate\Http\Request;

class CommitteesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Committees  $committees
     * @return \Illuminate\Http\Response
     */
    public function show(Committees $committees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Committees  $committees
     * @return \Illuminate\Http\Response
     */
    public function edit(Committees $committees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Committees  $committees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Committees $committees)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Committees  $committees
     * @return \Illuminate\Http\Response
     */
    public function destroy(Committees $committees)
    {
        //
    }
}
