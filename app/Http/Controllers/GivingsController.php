<?php

namespace App\Http\Controllers;

use App\Givings;
use Illuminate\Http\Request;

class GivingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Givings  $givings
     * @return \Illuminate\Http\Response
     */
    public function show(Givings $givings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Givings  $givings
     * @return \Illuminate\Http\Response
     */
    public function edit(Givings $givings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Givings  $givings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Givings $givings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Givings  $givings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Givings $givings)
    {
        //
    }
}
