<?php

namespace App\Http\Controllers;

use App\VisitTeam;
use Illuminate\Http\Request;

class VisitTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VisitTeam  $visitTeam
     * @return \Illuminate\Http\Response
     */
    public function show(VisitTeam $visitTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VisitTeam  $visitTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(VisitTeam $visitTeam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitTeam  $visitTeam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VisitTeam $visitTeam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VisitTeam  $visitTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(VisitTeam $visitTeam)
    {
        //
    }
}
