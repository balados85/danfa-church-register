<?php

namespace App\Http\Controllers;

use App\Visitations;
use Illuminate\Http\Request;

class VisitationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitations  $visitations
     * @return \Illuminate\Http\Response
     */
    public function show(Visitations $visitations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitations  $visitations
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitations $visitations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitations  $visitations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitations $visitations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitations  $visitations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitations $visitations)
    {
        //
    }
}
