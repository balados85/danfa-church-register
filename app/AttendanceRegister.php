<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AttendanceRegister extends Model{
   // use Notifiable;

    protected $fillable = ['week_of_year', 'day_of_week', 'total_attendance', 'register_date', 'closed', 'total_male', 'total_female', 'total_children', 'total_visitors', 'total_late_attendance', 'total_on_time_atendance']; 



    public function members(){

		return $this->belongsToMany(Members::class, 'attendances', 'register_id', 'member_id')->withPivot('late','present','time_in_church');
	}

	protected static function boot(){
        parent::boot();

        static::creating(function ($query) {
    	    $date = Carbon::now();

            $query->week_of_year = $date->weekOfYear;
            $query->day_of_week = $date->englishDayOfWeek;
            $query->register_date = Carbon::now();
            $query->closed = FALSE;
            $query->total_male = 0;
            $query->total_female = 0;
            $query->total_children = 0;
            $query->total_visitors = 0;
            $query->total_late_attendance =0;
            $query->total_on_time_atendance = 0;
        });
    }

}