<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class Members extends Model
{
    use Notifiable;

    protected $fillable = ['first_name', 'last_name', 'other_names', 'date_of_birth', 'gender', 'baptism_date', 'contact_number', 'email_address', 'address', 'marital_status', 'profession', 'home_town', 'place_of_work', 'status', 'image_directory']; 

    public function lives(){

		return $this->belongsTo(Zone::class);
	}

	public function in_committee(){

	 	return $this->belongsTo(Members::class);
	}

	public function visits(){

		return $this->belongsTo(Visitations::class);
	}

	public function receives(){

		return $this->hasMany(Benevolences::class);
	}

	public function donates(){

		return $this->hasMany(Donations::class);
	}

	public function records_giving(){

		return $this->hasMany(Givings::class);
	}	

	public function attended(){

		return $this->belongsTo(AttendaceRegister::class);
//return $this->belongsTo(AttendaceRegister::class)->using(Attendances::class)->as('attended')->withPivot(['present', 'late', 'time_at_church'])->withTimestamps();
	}

	/**
 * Accessor for Age.
 */
public function getAgeAttribute()
{
    return Carbon::parse($this->attributes['date_of_birth'])->age;
}

	protected static function boot()
{
    parent::boot();

    static::creating(function ($query) {
        $query->status = "ACTIVE";
        $query->image_directory = null;
        $query->profession = null;
    });
}

}
