<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benevolences extends Model
{
    use Notifiable;

    protected $fillable = ['amount_given', 'date_of_benevolence', 'purpose_of_benevolence', 'leader_id', 'notes_of_benevolnece']; 

    public function members(){

		return $this->belongsTo(Members::class);
	}
}
