<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committees extends Model
{
    use Notifiable;

    protected $fillable = ['committee_name', 'committee_leader', 'committee_purpose', 'committee_notes', 'leader_id']; 

    public function members(){

		return $this->hasMany(Members::class)->using(CommitteeMembers::class)->withPivot('active')->withTimestamps()->as('membership');
	}
}
