<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Visitors extends Model
{

    protected $fillable = ['last_name', 'other_names',  'contact_number', 'address', 'marital_status', 'date_of_visit', 'invited_by']; 

    protected static function boot()
{
    parent::boot();

    static::creating(function ($query) {
        $query->date_of_visit = Carbon::now();
    });
}

}
