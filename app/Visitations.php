<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitations extends Model
{
    use Notifiable;

    protected $fillable = ['visitee_id', 'purpose_of_visit', 'visit_leader_id', 'date_of_visit', 'visit_feedback']; 

   
	public function members(){

		return $this->belongsTo(Members::class)->using(VisitTeams::class)->as('visits')->withPivot('present');
	}	
}
