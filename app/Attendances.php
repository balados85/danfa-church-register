<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Attendances extends Model
{
    //use Notifiable;
    protected $fillable = ['present', 'late', 'member_id', 'register_id', 'time_in_church'];

	public $incrementing = true;

	protected static function boot()
{
    parent::boot();
    

    static::creating(function ($query) {
    	$date = Carbon::now();
        $query->time_in_church = $date->hour.':'.$date->minute;
    });
}
}
