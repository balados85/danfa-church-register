<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/members', 'MembersController@index')->name('members');

Route::get('/profile/{profile_id}', 'MembersController@profile')->name('profile');

Route::get('/addm', 'MembersController@create')->name('addm');

Route::post('/updatemember', 'MembersController@edit')->name('updatemember');

Route::post('/createmember', 'MembersController@store')->name('createmember');

Route::get('/zones', 'ZonesContoller@index')->name('zones');

Route::get('/zones/{zone_id}', 'ZonesContoller@show')->name('zone');

Route::post('/zones', 'ZonesContoller@store')->name('addz');

Route::put('/zones/{id}', 'ZonesContoller@update')->name('editz');

Route::get('/registers', 'AttendanceRegisterController@index')->name('indexreg');

Route::get('/register/{register}', 'AttendanceRegisterController@show')->name('showreg');

Route::get('/attendance',  'AttendanceRegisterController@store')->name('createreg');

Route::post('/attendances', 'AttendanceRegisterController@details')->name('attdetails');

Route::get('/details/{id}', 'AttendanceRegisterController@close_details')->name('closed');

Route::post('/recordattendance', 'AttendancesController@edit')->name('record');

Route::get('/visitor_n/{register_id}', 'AttendancesController@addvisitor')->name('linkv');

Route::get('/visitors', 'VisitorsController@index')->name('all');

Route::post('/visitor_add', 'VisitorsController@store')->name('addvi');

Route::post('/visitor_edit', 'VisitorsController@update')->name('editvi');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');