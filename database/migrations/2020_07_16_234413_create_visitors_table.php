<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('last_name');
            $table->string('other_names');
            $table->string('contact_number')->unique()->nullable();
            $table->string('address')->nullbale();
            $table->enum('marital_status', array('MARRIED', 'SINGLE', 'DIVORCED', 'WINDOWED'));
            $table->date('date_of_visit');
            $table->bigInteger('invited_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
