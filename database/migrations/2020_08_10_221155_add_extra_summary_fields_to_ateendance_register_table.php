<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraSummaryFieldsToAteendanceRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            $table->integer('total_male')->nullbale();
            $table->integer('total_female')->nullbale();
            $table->integer('total_children')->nullable();
            $table->integer('total_visitors')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            //
        });
    }
}
