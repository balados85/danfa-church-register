<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenevolencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benevolences', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->double('amount', 8, 2);
            $table->date('date_of_benevolence');
            $table->bigInteger('leader_id');
            $table->string('purpose_of_benevolence');
            $table->string('notes_of_benevolnece');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benevolences');
    }
}
