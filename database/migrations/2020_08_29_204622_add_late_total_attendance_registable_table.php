<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLateTotalAttendanceRegistableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            $table->integer('total_late_attendance')->nullable();
            $table->integer('total_on_time_atendance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            //
        });
    }
}
