<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WeekAndDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            $table->dropColumn('date_of_week');
            $table->dropColumn('week');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_registers', function (Blueprint $table) {
            $table->string('week_of_year');
            $table->integer('day_of_week');
        });
    }
}
