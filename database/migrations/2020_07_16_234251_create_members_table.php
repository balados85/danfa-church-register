<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('other_names')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('gender',array('MALE','FEMALE'));
            $table->date('baptism_date')->nullable();
            $table->string('contact_number')->unique()->nullable();
            $table->string('email_address')->unique()->nullable();
            $table->string('address');
            $table->enum('marital_status', array('MARRIED', 'SINGLE', 'DIVORCED', 'WIDOWED'));
            $table->string('profession')->nullable();
            $table->string('home_town')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('place_of_work')->nullable();
            $table->enum('status', array('ACTIVE','INACTIVE', 'MOVED', 'DISFELLOWSHIPPED'));
            $table->string('image_directory')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
