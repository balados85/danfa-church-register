@extends('layouts.create')

@section('content')
<div class="container">
  
      
    <div class="row justify-content-center">
          <nav class="nav flex-column">
            <a class="nav-link active" href="{{ url('/members') }}">Members</a>
            <a class="nav-link" href="{{ url('/register') }}">Attendance</a>
    </nav>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add new member') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('createmember') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('name') }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="othernames" class="col-md-4 col-form-label text-md-right">{{ __('Other Names') }}</label>

                            <div class="col-md-6">
                                <input id="othernames" type="text" class="form-control @error('othernames') is-invalid @enderror" name="othernames" value="{{ old('othernames') }}"  autocomplete="othernames" autofocus>

                                @error('othernames')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
 
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="gender" id="gender" value="MALE" autocomplete="off"> MALE
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="gender" id="gender" value="FEMALE" autocomplete="off"> FEMALE
                                    </label>
                                </div>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>   

                        <div class="form-group row">
                            <label for="maritalstatus" class="col-md-4 col-form-label text-md-right">{{ __('Marital status') }}</label>

                            <div class="col-md-6">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
 
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="MARRIED" autocomplete="off"> MARRIED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="SINGLE" autocomplete="off"> SINGLE
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="DIVORCED" autocomplete="off"> DIVORCED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="WIDOWED" autocomplete="off"> WIDOWED
                                    </label>
                                </div>
                                @error('maritalstatus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>   

                        <div class="form-group row">
                            <label for="dateofbirth" class="col-md-4 col-form-label text-md-right">{{ __('Date of birth') }}</label>

                            <div class="col-md-6">
                                <div class="input-group date">
                                    <input type="text" name="dateofbirth" class="form-control" value="12-02-2012" data-date-format="yyyy-mm-dd" required autocomplete="firstname" autofocus>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>

                                @error('dateofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="dateofbaptism" class="col-md-4 col-form-label text-md-right">{{ __('Date of baptism') }}</label>

                            <div class="col-md-6">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" name="dateofbaptism" class="form-control">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>

                                @error('dateofbaptism')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="contactnumber" class="col-md-4 col-form-label text-md-right">{{ __('Contact Number') }}</label>

                            <div class="col-md-6">
                                <input id="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" name="contactnumber" value="{{ old('contactnumber') }}" required autocomplete="contactnumber" autofocus>

                                @error('contactnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="houselocation" class="col-md-4 col-form-label text-md-right">{{ __('House Location') }}</label>

                            <div class="col-md-6">
                                <input id="houselocation" type="text" class="form-control @error('houselocation') is-invalid @enderror" name="houselocation" value="{{ old('houselocation') }}"  required autocomplete="houselocation" autofocus>

                                @error('houselocation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Profession') }}</label>

                            <div class="col-md-6">
                                <input id="profession" type="text" class="form-control @error('profession') is-invalid @enderror" name="profession" value="{{ old('profession') }}" autocomplete="profession" autofocus>

                                @error('profession')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="placeofwork" class="col-md-4 col-form-label text-md-right">{{ __('Place of work') }}</label>

                            <div class="col-md-6">
                                <input id="placeofwork" type="text" class="form-control @error('placeofwork') is-invalid @enderror" name="placeofwork" value="{{ old('placeofwork') }}"  autocomplete="placeofwork" autofocus>

                                @error('placeofwork')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hometown" class="col-md-4 col-form-label text-md-right">{{ __('Hometown') }}</label>

                            <div class="col-md-6">
                                <input id="hometown" type="text" class="form-control @error('hometown') is-invalid @enderror" name="hometown" value="{{ old('hometown') }}"  autocomplete="hometown" autofocus>

                                @error('hometown')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="placeofbirth" class="col-md-4 col-form-label text-md-right">{{ __('Place of birth') }}</label>

                            <div class="col-md-6">
                                <input id="placeofbirth" type="text" class="form-control @error('name') is-invalid @enderror" name="placeofbirth" value="{{ old('placeofbirth') }}"  autocomplete="name" autofocus>

                                @error('placeofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                      

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add member') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection