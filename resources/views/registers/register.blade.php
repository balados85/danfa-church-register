@extends('layouts.create')

@section('content')
<div class="container">

    <div class="row justify-content-center">
    	<nav class="nav flex-column">
  			<a class="nav-link active" href="{{ url('/members') }}">Members</a>
        <a class="nav-link" href="{{ url('/registers')}}">Attendance</a>
        <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
         <a class="nav-link" href="{{ url('/home')}}">Home</a>
	</nav>

        <div class="col-md-8">

            <div class="card">
                
                <div class="card-body">
                  <a class="btn btn-primary" href="{{route('createreg')}}" role="button">Create new register</a>
                  <br/>
                    <table class="table table-hover">
  						<thead class="thead-dark">
    						<tr>
      							<th scope="col">#</th>
      							<th scope="col">Day of week</th>
      							<th scope="col">Week </th>
      					      	<th scope="col">Date</th>
      					      	<th scope="col">Total Attendance</th>
                        <th scope="cpl">Status</th>
    						</tr>
  						</thead>
  						<tbody>
    						
    						@foreach($registers as $register)
    						<tr>
                    @if(!$register->closed)
                      <td><a href="{{ url('register', ['id' => $register->id]) }}">{{ $register->id }}</a></td>
                    @else
                      <td><a href="{{ url('details', ['id' => $register->id]) }}">{{ $register->id }}</a></td>
                    @endif
      							
      							<td>{{ $register->day_of_week }}</td>
      							<td>{{ $register->week_of_year }}</td>
      							<td>{{ $register->register_date }}</td>
      							<td>{{ $register->total_attendance }}</td>
                    @if($register->closed)
                      <td>CLOSED</td>
                    @else
                      <td>OPEN</td>
                    @endif
    						</tr>
    						@endforeach
  						</tbody>
					</table>
                </div>
                <?php echo $registers->render(); ?>
            </div>
        </div>
    </div>
</div>
@endsection

