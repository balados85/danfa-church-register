@extends('layouts.view')

@section('content')

@inject('present', 'App\Http\Controllers\AttendancesController')

@inject('late', 'App\Http\Controllers\AttendancesController')
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<body>
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="https://www.creative-tim.com/product/argon-dashboard" target="_blank">Attendance Summary</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <!--div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div-->
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name }} </span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 400px; background-image: url(https://raw.githack.com/creativetimofficial/argon-dashboard/master/assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-default opacity-8"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <!--div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-white">Hello Jesse</h1>
            <p class="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
            <a href="#!" class="btn btn-info">Edit profile</a>
          </div>
        </div-->
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <!--div class="card-profile-image">
                  <a href="#">
                    <img src="https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg" class="rounded-circle">
                  </a>
                </div-->
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
                <!--a href="#" class="btn btn-sm btn-info mr-4">Connect</a>
                <a href="#" class="btn btn-sm btn-default float-right">Message</a-->
              </div>
            </div>
            <div class="card-body pt-0 pt-md-4">
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_female}} </span>
                      <span class="description">Total female</span>
                    </div>
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_male}}</span>
                      <span class="description">Total male</span>
                    </div>
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_attendance}}</span>
                      <span class="description">Total attendance</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_visitors}} </span>
                      <span class="description">Total visitors</span>
                    </div>
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_late_attendance}}</span>
                      <span class="description">Late attendance</span>
                    </div>
                    <div>
                      <span class="heading">{{$pass_on['registers']->total_on_time_atendance}}</span>
                      <span class="description">On time attendance</span>
                    </div>
                    
                  </div>
                </div>
              </div>
              
              <div class="text-center">
               
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i>Week {{$pass_on['registers']->week_of_year}}
                </div>
                <div class="h5 mt-4">
                  <i class="ni business_briefcase-24 mr-2"></i>{{ $pass_on['registers']->day_of_week }}
                </div>
                <div>
                  <i class="ni education_hat mr-2"></i>{{ $pass_on['registers']->register_date }}
                </div>
                <hr class="my-4">
                <h3>Today's visitors</h3>
               
               <table class="table table-hover" id="membersTable">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Name of visitor</th>
                  <th scope="col">Invited by</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($pass_on['visitors'] as $visitor)
                 
                      <tr>

                        <th scope="row" >{{ $visitor->last_name.' '.$visitor->other_names }}</th>
                        <th scope="row" >{{ $visitor->invited_by}}</th>
                        
                      </tr>
                   
                
                @endforeach
            </tbody>
          </table>
                <!--p>Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.</p>
                <a href="#">Show more</a-->
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
            <div class="card bg-secondary shadow">
                <div class="card-header"><input type="text" class="form-control" id="search" placeholder="Search member..."></div>

                <div class="card-body">
                    
                    <table class="table table-hover" id="membersTable">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name of Member</th>
                  <th scope="col">Present</th>
                  <th scope="col">Late</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                
                @foreach($pass_on['registers']->members as $member)
                  
                    <form method="POST" action="{{ route('record') }}">
                          @csrf
                      <tr>

                        <th scope="row" >{{ $member->id }}</th>
                        <th scope="row" >{{ $member->first_name.' '.$member->other_names.' '.$member->last_name }}</th>
                        <td>
                          <div class="form-check">
                            <label class="form-check-label">
                              @if ($present::registered($member->id, $pass_on['registers']->id))
                                <input type="checkbox" class="form-check-input" value="1" name="present" checked/>
                              @else
                                <input type="checkbox" class="form-check-input" value="1" name="present"/>
                              @endif
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-check">
                             
                            <label class="form-check-label">
                              @if ($late::late($member->id, $pass_on['registers']->id))
                                <input type="checkbox" class="form-check-input" value="1" name="late" checked/>                       
                              @else
                                <input type="checkbox" class="form-check-input" value="1" name="late" />
                              @endif
                            </label>
                          </div>
                        </td>
                        <td>
                          <input type="hidden" name = "member_id" value="{{$member->id}}"/>
                          <input type="hidden" name = "register_id" value="{{$pass_on['registers']->id}}"/>
                           <button type="submit" class="btn btn-info">
                                          {{ __('Update') }}
                          </button>
                        </td>
                      </tr>
                    </form>
                
                @endforeach
              </tbody>
            </table>
           
                </div>
            </div>
        </div>
    </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6 m-auto text-center">
        <div class="copyright">
          <!--p>Made with <a href="https://www.creative-tim.com/product/argon-dashboard" target="_blank">Argon Dashboard</a> by Creative Tim</p-->
        </div>
      </div>
    </div>
  </footer>
</body>

@endsection
