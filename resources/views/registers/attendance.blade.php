@extends('layouts.list')

@section('content')

@inject('provider', 'App\Http\Controllers\AttendancesController')
<div class="container">

  <div class="row justify-content-center">
    <nav class="nav flex-column">
  		<a class="nav-link active" href="{{ url('/registers') }}">Attendance Reports</a>
      <a class="nav-link" href="{{ url('/members')}}">Members</a>
      <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
       <a class="nav-link" href="{{ url('/home')}}">Home</a>
	  </nav>

      <div class="col-md-8">
        <div class="form-group pull">
  
  <input type="text" class="form-control" id="search" placeholder="Search member...">

  <br/>

  <form method="POST" action="{{ route('attdetails') }}">
    @csrf
    <input type="hidden" class="form-check-input" value = "{{$registers->id}}" name="close" />
      <button type="submit" class="btn btn-info" >
          {{ __('Close Register') }}
      </button>
  </form>
<nav class="nav flex-column">
        <a class="nav-link active" href="{{ url('visitor_n', ['id' => $registers->id]) }}">Add visitor</a>
  </nav>
</div>
        <div class="card">      
          <div class="card-body">
            <table class="table table-hover" id="membersTable">
  						<thead class="thead-dark">
    						<tr>
      						<th scope="col">#</th>
      						<th scope="col">Name of Member</th>
      						<th scope="col">Present</th>
      					  <th scope="col">Late</th>
                  <th scope="col"></th>
    						</tr>
  						</thead>
  						<tbody>
    						
    						@foreach($registers->members as $member)
                  @if (!$provider::registered($member->id, $registers->id))
                    <form method="POST" action="{{ route('record') }}">
                          @csrf
          					  <tr>

            						<th scope="row" >{{ $member->id }}</th>
            						<th scope="row" >{{ $member->first_name.' '.$member->other_names.' '.$member->last_name }}</th>
            						<td>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input" value="1" name="present"/>
                            </label>
                          </div>
                        </td>
            						<td>
                          <div class="form-check">
                            
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input" value="1" name="late"/>
                            </label>
                          </div>
                        </td>
                        <td>
                          <input type="hidden" name = "member_id" value="{{$member->id}}"/>
                          <input type="hidden" name = "register_id" value="{{$registers->id}}"/>
                           <button type="submit" class="btn btn-info">
                                          {{ __('Record') }}
                          </button>
                        </td>
          					  </tr>
                    </form>
                  @endif
    						@endforeach
  						</tbody>
					  </table>

        </div>     
      </div>
    </div>
  </div>
</div>
@endsection