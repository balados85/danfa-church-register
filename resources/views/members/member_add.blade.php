@extends('layouts.view')


@section('content')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 400px; background-image: url(https://raw.githack.com/creativetimofficial/argon-dashboard/master/assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-8"></span>
  <!-- Header container -->
  <div class="container-fluid d-flex align-items-center">
    <div class="row">
      <div class="col-lg-7 col-md-10">
        <h1 class="display-2 text-white">Hello Jesse</h1>
        <p class="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
        <a href="#!" class="btn btn-info">Edit profile</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid mt--7">


    <div class="row justify-content-center">
      <div class="col-md-4">
        <div class="card bg-secondary shadow">
        <div class="card-header">
          <h3 class="mb-0">Manage</h3></div>

          <div class="card-body">
        <nav class="nav flex-column">
        <a class="nav-link active" href="{{ url('/members') }}">Members</a>
        <a class="nav-link" href="{{ url('/register') }}">Attendance</a>
        <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
         <a class="nav-link" href="{{ url('/home')}}">Home</a>
      </nav>
    </div>
    </div>



  </div>
        <div class="col-md-8">
            <div class="card bg-secondary shadow">
                <div class="card-header"><h3 class="mb-0">{{ __('Add new member') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('createmember') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="firstname" class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}</label>
                                <input id="firstname" type="text" placeholder="Maxwell" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('name') }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="lastname" class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}</label>
                                <input id="lastname" type="text" placeholder="Opoku" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                              <label for="othernames" class="col-md-6 col-form-label text-md-left">{{ __('Other Names') }}</label>
                                <input id="othernames" type="text" placeholder="Hans" class="form-control @error('othernames') is-invalid @enderror" name="othernames" value="{{ old('othernames') }}"  autocomplete="othernames" autofocus>

                                @error('othernames')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <label for="gender" class="col-md-9 col-form-label text-md-left">{{ __('Gender') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">


                                    <label class="btn btn-secondary">
                                        <input type="radio" name="gender" id="gender" value="MALE" autocomplete="on"> MALE
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="gender" id="gender" value="FEMALE" autocomplete="off"> FEMALE
                                    </label>

                                </div>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <label for="maritalstatus" class="col-md-6 col-form-label text-left">{{ __('Marital status') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="MARRIED" autocomplete="off"> MARRIED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="SINGLE" autocomplete="off"> SINGLE
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="DIVORCED" autocomplete="off"> DIVORCED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="WIDOWED" autocomplete="off"> WIDOWED
                                    </label>
                                </div>
                                @error('maritalstatus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>




                        </div>

                        <div class="form-group row">


                            <div class="col-md-6">
                                <label for="dateofbirth" class="col-md-6 col-form-label text-md-left">{{ __('Date of birth') }}</label>
                                <div class="input-group date">
                                    <input type="date" name="dateofbirth" class="form-control" value="12-02-2012" data-date-format="yyyy-mm-dd" required autocomplete="firstname" autofocus>

                                </div>

                                @error('dateofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                                <label for="dateofbaptism" class="col-md-6 col-form-label text-md-left">{{ __('Date of baptism') }}</label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="date" name="dateofbaptism" class="form-control">

                                </div>

                                @error('dateofbaptism')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                              <label for="placeofbirth" class="col-md-6 col-form-label text-md-left">{{ __('Place of birth') }}</label>

                                <input id="placeofbirth" type="text" class="form-control @error('name') is-invalid @enderror" name="placeofbirth" value="{{ old('placeofbirth') }}"  autocomplete="name" autofocus>

                                @error('placeofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                        </div>

                         <div class="form-group row">

                        </div>

                        <hr class="my-4">
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>

                        <div class="form-group row">


                            <div class="col-md-6">
                              <label for="contactnumber" class="col-md-6 col-form-label text-md-left">{{ __('Contact Number') }}</label>
                                <input id="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" name="contactnumber" value="{{ old('contactnumber') }}" required autocomplete="contactnumber" autofocus>

                                @error('contactnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                              <label for="email" class="col-md-6 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">


                            <div class="col-md-6">
                              <label for="houselocation" class="col-md-6 col-form-label text-md-left">{{ __('House Location') }}</label>
                                <input id="houselocation" type="text" class="form-control @error('houselocation') is-invalid @enderror" name="houselocation" value="{{ old('houselocation') }}"  required autocomplete="houselocation" autofocus>

                                @error('houselocation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                              <label for="name" class="col-md-6 col-form-label text-md-left">{{ __('Profession') }}</label>
                                <input id="profession" type="text" class="form-control @error('profession') is-invalid @enderror" name="profession" value="{{ old('profession') }}" autocomplete="profession" autofocus>

                                @error('profession')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">


                            <div class="col-md-6">
                              <label for="placeofwork" class="col-md-6 col-form-label text-md-left">{{ __('Place of work') }}</label>
                                <input id="placeofwork" type="text" class="form-control @error('placeofwork') is-invalid @enderror" name="placeofwork" value="{{ old('placeofwork') }}"  autocomplete="placeofwork" autofocus>

                                @error('placeofwork')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>




                            <div class="col-md-6">
                                <label for="hometown" class="col-md-6 col-form-label text-md-left">{{ __('Hometown') }}</label>
                                <input id="hometown" type="text" class="form-control @error('hometown') is-invalid @enderror" name="hometown" value="{{ old('hometown') }}"  autocomplete="hometown" autofocus>

                                @error('hometown')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-info">
                                    {{ __('Add member') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
