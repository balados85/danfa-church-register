@extends('layouts.list')

@section('content')
<div class="container">

    <div class="row justify-content-center">
    	<nav class="nav flex-column">
  			<a class="nav-link active" href="{{ url('/addm') }}">Add Members</a>
  			<a class="nav-link" href="{{ url('/registers') }}">Attendance</a>
         <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
         <a class="nav-link" href="{{ url('/home')}}">Home</a>
	</nav>

        <div class="col-md-8">
          <div class="form-group">
  
  <input type="text" class="form-control" id="search" placeholder="Search member...">
</div>
            <div class="card">
                
                <div class="card-body">
                  
                    <table class="table table-hover table-bordered table-striped" id="membersTable">
  						<thead class="thead-dark">
    						<tr>
      							<th scope="col">#</th>
      							<th scope="col">First</th>
      							<th scope="col">Last</th>
      					      	<th scope="col">Gender</th>
      					      	<th scope="col">Status</th>
    						</tr>
  						</thead>
  						<tbody>
    						
    						@foreach($members as $member)
    						<tr>
      							<th scope="row" ><a href="{{route('profile', ['profile_id' => Crypt::encrypt($member->id) ])}}">{{ $member->id }}</a></th>
      							<td>{{ $member->first_name }}</td>
      							<td>{{ $member->last_name}}</td>
      							<td>{{ $member->gender }}</td>
      							<td>{{ $member->status }}</td>
    						</tr>
    						@endforeach
  						</tbody>
					</table>
                </div>
                <?php echo $members->render(); ?>
            </div>
        </div>
    </div>
</div>
@endsection

