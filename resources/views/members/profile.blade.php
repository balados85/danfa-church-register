@extends('layouts.view')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<body>
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="https://www.creative-tim.com/product/argon-dashboard" target="_blank">Member profile</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <!--div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div-->
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name }} </span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 400px; background-image: url(https://raw.githack.com/creativetimofficial/argon-dashboard/master/assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-default opacity-8"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-white">Hello Jesse</h1>
            <p class="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
            <a href="#!" class="btn btn-info">Edit profile</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg" class="rounded-circle">
                  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
                <a href="#" class="btn btn-sm btn-info mr-4">Connect</a>
                <a href="#" class="btn btn-sm btn-default float-right">Message</a>
              </div>
            </div>
            <div class="card-body pt-0 pt-md-4">
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div>
                      <span class="heading">{{$member->marital_status}} </span>
                      <span class="description">marital status</span>
                    </div>
                    <div>
                      <span class="heading">{{$member->gender}}</span>
                      <span class="description">Gender</span>
                    </div>
                    <div>
                      <span class="heading">{{$member->status}}</span>
                      <span class="description">Attendance</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-center">
                <h3>
                  {{ $member->first_name }} {{ $member->other_names }} {{ $member->last_name }}<span class="font-weight-light">, {{ $member->age }}</span>
                </h3>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i>{{$member->date_of_baptism}}
                </div>
                <div class="h5 mt-4">
                  <i class="ni business_briefcase-24 mr-2"></i>{{ $member->profession }}
                </div>
                <div>
                  <i class="ni education_hat mr-2"></i>{{ $member->place_of_work }}
                </div>
                <hr class="my-4">
                <!--p>Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.</p>
                <a href="#">Show more</a-->
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
            <div class="card bg-secondary shadow">
                <div class="card-header"><h3 class="mb-0">{{ __('Update member detail') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('updatemember') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="firstname" class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}</label>
                                <input value="{{$member->id}}" name="id"/ hidden>
                                <input id="firstname" type="text"  class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ $member->first_name }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="lastname" class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}</label>
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ $member->last_name }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                              <label for="othernames" class="col-md-6 col-form-label text-md-left">{{ __('Other Names') }}</label>
                                <input id="othernames" type="text"  class="form-control @error('othernames') is-invalid @enderror" name="othernames" value="{{ $member->other_name }}"  autocomplete="othernames" autofocus>

                                @error('othernames')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <label for="gender" class="col-md-9 col-form-label text-md-left">{{ __('Gender') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">


                                    <label class="btn btn-secondary">
                                      @if($member->gender==="MALE")
                                        <input type="radio" name="gender" id="gender" value="MALE" checked autocomplete="on"> MALE
                                      @else
                                        <input type="radio" name="gender" id="gender" value="MALE"  autocomplete="on"> MALE
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                      @if($member->gender==="FEMALE")
                                        <input type="radio" name="gender" id="gender" value="FEMALE" checked autocomplete="off"> 
                                        FEMALE
                                      @else
                                       <input type="radio" name="gender" id="gender" value="FEMALE" autocomplete="off"> 
                                        FEMALE
                                      @endif
                                    </label>

                                </div>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="maritalstatus" class="col-md-6 col-form-label text-left">{{ __('Marital status') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                    <label class="btn btn-secondary">
                                      @if($member->marital_status==="MARRIED")
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="MARRIED" checked autocomplete="off"> MARRIED
                                      @else
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="MARRIED" autocomplete="off"> MARRIED
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                     @if($member->marital_status==="SINGLE")
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="SINGLE" checked autocomplete="off"> SINGLE
                                      @else
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="SINGLE" autocomplete="off"> SINGLE
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                      @if($member->marital_status==="DIVORCED")
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="DIVORCED" checked autocomplete="off"> DIVORCED
                                      @else
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="DIVORCED" autocomplete="off"> DIVORCED
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                      @if($member->marital_status==="WIDOWED")
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="WIDOWED" checked autocomplete="off"> WIDOWED
                                      @else
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="WIDOWED" autocomplete="off"> WIDOWED
                                      @endif
                                    </label>
                                </div>
                                @error('maritalstatus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          </div>

                          <div class="form-group row">
                            <div class="col-md-6">
                                <label for="maritalstatus" class="col-md-6 col-form-label text-left">{{ __('Attendance status') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                    <label class="btn btn-secondary">
                                      @if($member->status==="ACTIVE")
                                        <input type="radio" name="activestatus" id="activestatus" value="ACTIVE" checked autocomplete="off"> ACTIVE
                                      @else
                                        <input type="radio" name="activestatus" id="activestatus" value="ACTIVE" autocomplete="off"> ACTIVE
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                     @if($member->status==="INACTIVE")
                                        <input type="radio" name="activestatus" id="activestatus" value="INACTIVE" checked autocomplete="off"> INACTIVE
                                      @else
                                        <input type="radio" name="activestatus" id="activestatus" value="INACTIVE" autocomplete="off"> INACTIVE
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                      @if($member->status==="MOVED")
                                        <input type="radio" name="activestatus" id="activestatus" value="MOVED" checked autocomplete="off"> MOVED
                                      @else
                                        <input type="radio" name="activestatus" id="activestatus" value="MOVED" autocomplete="off"> MOVED
                                      @endif
                                    </label>
                                    <label class="btn btn-secondary">
                                      @if($member->status==="DISFELLOWSHIPPED")
                                        <input type="radio" name="activestatus" id="activestatus" value="DISFELLOWSHIPPED" checked autocomplete="off"> DISFELLOWSHIPPED
                                      @else
                                        <input type="radio" name="activestatus" id="activestatus" value="DISFELLOWSHIPPED" autocomplete="off"> DISFELLOWSHIPPED
                                      @endif
                                    </label>
                                </div>
                                @error('maritalstatus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                          </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="dateofbirth" class="col-md-6 col-form-label text-md-left">{{ __('Date of birth') }}</label>
                                <div class="input-group date">
                                    <input type="date" name="dateofbirth" class="form-control" value="{{$member->date_of_birth}}" data-date-format="yyyy-mm-dd" required autocomplete="firstname" autofocus>
                                </div>
                                 @error('dateofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                                <label for="dateofbaptism" class="col-md-6 col-form-label text-md-left">{{ __('Date of baptism') }}</label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="date" name="dateofbaptism" class="form-control" value="{{$member->date_of_baptism}}">
                                </div>
                                @error('dateofbaptism')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                              <label for="placeofbirth" class="col-md-6 col-form-label text-md-left">{{ __('Place of birth') }}</label>

                                <input id="placeofbirth" type="text" class="form-control @error('name') is-invalid @enderror" name="placeofbirth" value="{{ $member->place_of_birth}}"  autocomplete="name" autofocus>
                                @error('placeofbirth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">

                        </div>

                        <hr class="my-4">
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>

                        <div class="form-group row">


                            <div class="col-md-6">
                              <label for="contactnumber" class="col-md-6 col-form-label text-md-left">{{ __('Contact Number') }}</label>
                                <input id="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" name="contactnumber" value="{{ $member->contact_number }}" required autocomplete="contactnumber" autofocus>
                                @error('contactnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="col-md-6">
                              <label for="email" class="col-md-6 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $member->email_address }}" autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6">
                              <label for="houselocation" class="col-md-6 col-form-label text-md-left">{{ __('House Location') }}</label>
                                <input id="houselocation" type="text" class="form-control @error('houselocation') is-invalid @enderror" name="houselocation" value="{{ $member->address }}"  required autocomplete="houselocation" autofocus>
                                @error('houselocation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                              <label for="name" class="col-md-6 col-form-label text-md-left">{{ __('Profession') }}</label>
                                <input id="profession" type="text" class="form-control @error('profession') is-invalid @enderror" name="profession" value="{{ $member->profession }}" autocomplete="profession" autofocus>
                                @error('profession')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                              <label for="placeofwork" class="col-md-6 col-form-label text-md-left">{{ __('Place of work') }}</label>
                                <input id="placeofwork" type="text" class="form-control @error('placeofwork') is-invalid @enderror" name="placeofwork" value="{{ $member->place_of_work }}"  autocomplete="placeofwork" autofocus>
                                @error('placeofwork')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <label for="hometown" class="col-md-6 col-form-label text-md-left">{{ __('Hometown') }}</label>
                                <input id="hometown" type="text" class="form-control @error('hometown') is-invalid @enderror" name="hometown" value="{{ $member->home_town  }}"  autocomplete="hometown" autofocus>
                                @error('hometown')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-info">
                                    {{ __('Update member') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6 m-auto text-center">
        <div class="copyright">
          <!--p>Made with <a href="https://www.creative-tim.com/product/argon-dashboard" target="_blank">Argon Dashboard</a> by Creative Tim</p-->
        </div>
      </div>
    </div>
  </footer>
</body>

@endsection
