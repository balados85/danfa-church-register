@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
                <div class="content">
                
                <div class="links">
                    <a href="{{ url('/members') }}">Members</a>
                    <a href="{{ url('/finances') }}">Finances</a>
                    <a href="{{ url('/committees') }}">Committees</a>
                    <a href="{{ url('/visitors') }}">Visitors</a>
                    <a href="{{ url('/zones') }}">Zones</a>
                    <a href="{{ url('/registers') }}">Attendance Register</a>
                    <a href="{{ url('/users') }}">Users</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
