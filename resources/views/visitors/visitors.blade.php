@extends('layouts.list')

@section('content')
<div class="container">

    <div class="row justify-content-center">
    	<nav class="nav flex-column">
  			<a class="nav-link active" href="{{ url('/addm') }}">Add Members</a>
  			<a class="nav-link" href="{{ url('/registers') }}">Attendance</a>
        <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
        <a class="nav-link" href="{{ url('/home')}}">Home</a>
	</nav>

        <div class="col-md-8">
          <div class="form-group">
  
  <input type="text" class="form-control" id="search" placeholder="Search member...">
</div>
            <div class="card">
                
                <div class="card-body">
                  
                    <table class="table table-hover table-bordered table-striped" id="membersTable">
  						<thead class="thead-dark">
    						<tr>
      							<th scope="col">#</th>
      							<th scope="col">Name of Visitor</th>
      							<th scope="col">Invited by</th>
      					     <th scope="col">Contact</th>
      					     <th scope="col">Date of visit</th>
                     <th scope="col">Marital status</th>
    						</tr>
  						</thead>
  						<tbody>
    						
    						@foreach($visitors as $visitor)
    						<tr>
      							<th scope="row">{{ $visitor->id }}</th>
      							<td>{{ $visitor->other_names }} {{ $visitor->last_name }}</td>
      							<td>{{ $visitor->invited_by}}</td>
      							<td>{{ $visitor->contact_number }}</td>
      							<td>{{ $visitor->date_of_visit }}</td>
                    <td>{{ $visitor->address }}</td>
    						</tr>
    						@endforeach
  						</tbody>
					</table>
                </div>
                <?php echo $visitors->render(); ?>
            </div>
        </div>
    </div>
</div>
@endsection

