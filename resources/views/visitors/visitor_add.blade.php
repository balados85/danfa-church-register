@extends('layouts.view')


@section('content')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 400px; background-image: url(https://raw.githack.com/creativetimofficial/argon-dashboard/master/assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-8"></span>
  <!-- Header container -->
 
</div>
<div class="container-fluid mt--7">


    <div class="row justify-content-center">
      <div class="col-md-4">
        <div class="card bg-secondary shadow">
        <div class="card-header">
          <h3 class="mb-0">Manage</h3></div>

          <div class="card-body">
        <nav class="nav flex-column">
        <a class="nav-link active" href="{{ url('/members') }}">Members</a>
        <a class="nav-link" href="{{ url('/registers') }}">Attendance</a>
        <a class="nav-link" href="{{ url('/visitors')}}">Visitors</a>
         <a class="nav-link" href="{{ url('/home')}}">Home</a>
      </nav>
    </div>
    </div>



  </div>
        <div class="col-md-8">
            <div class="card bg-secondary shadow">
                <div class="card-header"><h3 class="mb-0">{{ __('Add new visitor') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('addvi') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="lastname" class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}</label>
                                <input id="lastname" type="text" placeholder="Maxwell" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('name') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                              <label for="othernames" class="col-md-6 col-form-label text-md-left">{{ __('Other Names') }}</label>
                                <input id="othernames" type="text" placeholder="Hans" class="form-control @error('othernames') is-invalid @enderror" name="othernames" value="{{ old('othernames') }}"  autocomplete="othernames" autofocus>

                                @error('othernames')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            

                            <div class="col-md-6">
                              <label for="invitedby" class="col-md-6 col-form-label text-md-left">{{ __('Invited by') }}</label>
                                <input id="invitedby" type="text" placeholder="Hans" class="form-control @error('invitedby') is-invalid @enderror" name="invitedby" value="{{ old('invitedby') }}"  autocomplete="invitedby" autofocus>

                                @error('invitedby')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <label for="maritalstatus" class="col-md-6 col-form-label text-left">{{ __('Marital status') }}</label>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="MARRIED" autocomplete="off"> MARRIED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="SINGLE" autocomplete="off"> SINGLE
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="DIVORCED" autocomplete="off"> DIVORCED
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="WIDOWED" autocomplete="off"> WIDOWED
                                    </label>
                                </div>
                                @error('maritalstatus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">

                        </div>

                        <hr class="my-4">
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>

                        <div class="form-group row">


                            <div class="col-md-6">
                              <label for="contactnumber" class="col-md-6 col-form-label text-md-left">{{ __('Contact Number') }}</label>
                                <input id="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" name="contactnumber" value="{{ old('contactnumber') }}" required autocomplete="contactnumber" autofocus>

                                @error('contactnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                              <label for="address" class="col-md-6 col-form-label text-md-left">{{ __('Address') }}</label>
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <input type="hidden" name="register_id" value="{{$id}}"/>
                                <button type="submit" class="btn btn-info">
                                    {{ __('Add Visitors') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
